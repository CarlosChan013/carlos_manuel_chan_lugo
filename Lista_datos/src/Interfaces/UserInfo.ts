export interface UserInfo {
    Id : number;
    Name: string;
    Lastname: string;
    Email: string;
    Age : number;
}