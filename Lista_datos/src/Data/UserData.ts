import type { UserInfo } from "@/Interfaces/UserInfo";

const Users: UserInfo []= [
    {
        Id : 1,
        Name: 'Carlos',
        Lastname: 'Chan',
        Email: 'Carlos@gmail.com',
        Age : 21
    },
    
    {
        Id : 2,
        Name: 'Mayrin',
        Lastname: 'Arcos',
        Email: 'Mayrin@gmail.com',
        Age : 20  
    },
    {
        Id : 3,
        Name: 'Leslie',
        Lastname: 'Nieves',
        Email: 'Leslie@gmail.com',
        Age : 21  
    },
    {
        Id : 4,
        Name: 'Sharon',
        Lastname: 'Sanchez',
        Email: 'Sharon@gmail.com',
        Age : 21  
    },
    {
        Id : 5,
        Name: 'Jenny',
        Lastname: 'Tec',
        Email: 'Jenny@gmail.com',
        Age : 20 
    },
    {
        Id : 6,
        Name: 'Melissa',
        Lastname: 'Miron',
        Email: 'Melissa@gmail.com',
        Age : 22
    },
    {
        Id : 7,
        Name: 'Naydelin',
        Lastname: 'Kumul',
        Email: 'Naydelin@gmail.com',
        Age : 22
    },
    {
        Id : 8,
        Name: 'Andrea',
        Lastname: 'Colli',
        Email: 'Andrea@gmail.com',
        Age : 21
    },
    {
        Id : 9,
        Name: 'Esmeralda',
        Lastname: 'Flores',
        Email: 'Esme@gmail.com',
        Age : 22
    },
    {
        Id : 10,
        Name: 'Jocelyn',
        Lastname: 'Quijano',
        Email: 'Jocelyn@gmail.com',
        Age : 22
    }

]
export default Users;